﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using PerioTcpRdrBase;
using PerioTCPRdr;
using System.Collections;
using TCPReader;
using TCPReader.Properties;
using System.Configuration.Assemblies;



namespace TCPReader
{

    
    public partial class frmZamanKisitTablosu : Form
    {
        public frmZamanKisitTablosu()
        {
            InitializeComponent();
        }


        public static string timetoStr(DateTime inComingTime)
        {
            string returned="";
            int second;
            int hour;
            int minute;
            string strSecond;
            string strHour;
            string strMinute;

            hour = inComingTime.Hour;
            second = inComingTime.Second;
            minute = inComingTime.Minute;

            if (Convert.ToString(hour).Length < 2) { strHour = "0" + Convert.ToString(hour); } else { strHour = Convert.ToString(hour); }
            if (Convert.ToString(minute).Length < 2) { strMinute = "0" + Convert.ToString(minute); } else { strMinute = Convert.ToString(minute); }
            if (Convert.ToString(second).Length < 2) { strSecond = "0" + Convert.ToString(second); } else { strSecond = Convert.ToString(second); }


            returned = strHour + ":" + strMinute;

            return returned;
        }


        void rAddLog(string str)
        {
            logText.AppendText(str);
        }


        private void button2_Click(object sender, EventArgs e)
        {

            TTACList TACList;
            TACList = new TTACList();


            if (frmMain.rdr.Connected == true)
            {

                try
                {
                    if (frmMain.rdr.GetTimeConstraintTables((byte)edtTabloAdi.Value, out TACList) == true)
                    {
                        edtListeAdi.Text = TACList.Name;

                        for (int i = 0; i <= 7; i++)
                        {

                            switch (i)
                            {

                                case 0:
                                tabloVerileri1SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri1SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 1:
                                tabloVerileri2SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri2SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 2:
                                tabloVerileri3SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri3SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 3:
                                tabloVerileri4SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri4SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 4:
                                tabloVerileri5SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri5SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 5:
                                tabloVerileri6SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri6SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 6:
                                tabloVerileri7SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri7SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;

                                case 7:
                                tabloVerileri8SatirBaslangic.Text = timetoStr(TACList.Part[i].StartTime).Replace("01.01.2000 ", "");
                                tabloVerileri8SatirBitis.Text = timetoStr(TACList.Part[i].EndTime).Replace("01.01.2000 ", "");
                                break;


                                default:
                                break;
                            }


                        }

                    }

                }
                catch (Exception hata)
                {

                    rAddLog("Bilgiler getirilemedi"+  hata.Message);
                }


            }
            else {
                rAddLog("Cihazla bağlantı kurulamadı.");
            }

            


        }

        private void button1_Click(object sender, EventArgs e)
        {

            TTACList TACList;
            TACList = new TTACList();


            if (frmMain.rdr.Connected == true)
            {
                TACList.Name = edtListeAdi.Text;

                for (int i = 0; i <= 7; i++)
                {


                    switch (i)
                    {
                     
                       case 0:
                            break;
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri1SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri1SatirBitis.Text);
                       case 1:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri2SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri2SatirBitis.Text);
                           break;
                       case 2:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri3SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri3SatirBitis.Text);
                            break;
                       case 3:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri4SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri4SatirBitis.Text);
                            break;
                       case 4:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri5SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri5SatirBitis.Text);
                           break;
                       case 5:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri6SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri6SatirBitis.Text);
                           break;
                       case 6:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri7SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri7SatirBitis.Text);
                           break;
                       case 7:
                            TACList.Part[i].StartTime = Convert.ToDateTime(tabloVerileri8SatirBaslangic.Text);
                            TACList.Part[i].EndTime = Convert.ToDateTime(tabloVerileri8SatirBitis.Text);
                           break;

                        default:
                        break;
                    }

                    }
                    try
                    {
                        if (frmMain.rdr.SetTimeConstraintTables((byte)edtTabloAdi.Value, TACList) == true)
                        { rAddLog("Zaman Kısıt Tablosu gönderildi."); }
                        else { rAddLog("Zaman Kısıt Tablosu gönderilemedi."); }

                    }
                    catch (Exception hata)
                    {

                        rAddLog("Zaman Kısıt Tablosu gönderilemedi." + hata.Message);
                    }

              
                


            }
            else {rAddLog("Cihazla bağlantı kurulamadı.");}



        }
    }
}
