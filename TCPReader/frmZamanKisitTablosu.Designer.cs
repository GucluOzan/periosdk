﻿namespace TCPReader
{
    partial class frmZamanKisitTablosu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.edtTabloAdi = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.edtListeAdi = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabloVerileri1SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri2SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri4SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri3SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri6SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri5SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri8SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri7SatirBaslangic = new System.Windows.Forms.TextBox();
            this.tabloVerileri8SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri7SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri6SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri5SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri4SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri3SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri2SatirBitis = new System.Windows.Forms.TextBox();
            this.tabloVerileri1SatirBitis = new System.Windows.Forms.TextBox();
            this.logText = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.edtTabloAdi)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // edtTabloAdi
            // 
            this.edtTabloAdi.Location = new System.Drawing.Point(68, 12);
            this.edtTabloAdi.Maximum = new decimal(new int[] {
            65,
            0,
            0,
            0});
            this.edtTabloAdi.Name = "edtTabloAdi";
            this.edtTabloAdi.Size = new System.Drawing.Size(94, 20);
            this.edtTabloAdi.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Liste Adı : ";
            // 
            // edtListeAdi
            // 
            this.edtListeAdi.Location = new System.Drawing.Point(68, 38);
            this.edtListeAdi.Name = "edtListeAdi";
            this.edtListeAdi.Size = new System.Drawing.Size(184, 20);
            this.edtListeAdi.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabloVerileri8SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri7SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri6SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri5SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri4SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri3SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri2SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri1SatirBitis);
            this.groupBox1.Controls.Add(this.tabloVerileri8SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri7SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri6SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri5SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri4SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri3SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri2SatirBaslangic);
            this.groupBox1.Controls.Add(this.tabloVerileri1SatirBaslangic);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(16, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 334);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tablo Verileri";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Başlangıç";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(149, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Bitiş";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "1. Satır : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "2. Satır : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "3. Satır : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "4. Satır : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "5. Satır : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "6. Satır : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 232);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "7. Satır : ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 264);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "8. Satır : ";
            // 
            // tabloVerileri1SatirBaslangic
            // 
            this.tabloVerileri1SatirBaslangic.Location = new System.Drawing.Point(61, 42);
            this.tabloVerileri1SatirBaslangic.Name = "tabloVerileri1SatirBaslangic";
            this.tabloVerileri1SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri1SatirBaslangic.TabIndex = 10;
            this.tabloVerileri1SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri2SatirBaslangic
            // 
            this.tabloVerileri2SatirBaslangic.Location = new System.Drawing.Point(61, 73);
            this.tabloVerileri2SatirBaslangic.Name = "tabloVerileri2SatirBaslangic";
            this.tabloVerileri2SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri2SatirBaslangic.TabIndex = 11;
            this.tabloVerileri2SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri4SatirBaslangic
            // 
            this.tabloVerileri4SatirBaslangic.Location = new System.Drawing.Point(61, 134);
            this.tabloVerileri4SatirBaslangic.Name = "tabloVerileri4SatirBaslangic";
            this.tabloVerileri4SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri4SatirBaslangic.TabIndex = 13;
            this.tabloVerileri4SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri3SatirBaslangic
            // 
            this.tabloVerileri3SatirBaslangic.Location = new System.Drawing.Point(61, 103);
            this.tabloVerileri3SatirBaslangic.Name = "tabloVerileri3SatirBaslangic";
            this.tabloVerileri3SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri3SatirBaslangic.TabIndex = 12;
            this.tabloVerileri3SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri6SatirBaslangic
            // 
            this.tabloVerileri6SatirBaslangic.Location = new System.Drawing.Point(61, 198);
            this.tabloVerileri6SatirBaslangic.Name = "tabloVerileri6SatirBaslangic";
            this.tabloVerileri6SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri6SatirBaslangic.TabIndex = 15;
            this.tabloVerileri6SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri5SatirBaslangic
            // 
            this.tabloVerileri5SatirBaslangic.Location = new System.Drawing.Point(61, 167);
            this.tabloVerileri5SatirBaslangic.Name = "tabloVerileri5SatirBaslangic";
            this.tabloVerileri5SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri5SatirBaslangic.TabIndex = 14;
            this.tabloVerileri5SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri8SatirBaslangic
            // 
            this.tabloVerileri8SatirBaslangic.Location = new System.Drawing.Point(61, 260);
            this.tabloVerileri8SatirBaslangic.Name = "tabloVerileri8SatirBaslangic";
            this.tabloVerileri8SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri8SatirBaslangic.TabIndex = 17;
            this.tabloVerileri8SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri7SatirBaslangic
            // 
            this.tabloVerileri7SatirBaslangic.Location = new System.Drawing.Point(61, 229);
            this.tabloVerileri7SatirBaslangic.Name = "tabloVerileri7SatirBaslangic";
            this.tabloVerileri7SatirBaslangic.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri7SatirBaslangic.TabIndex = 16;
            this.tabloVerileri7SatirBaslangic.Text = "00:00";
            // 
            // tabloVerileri8SatirBitis
            // 
            this.tabloVerileri8SatirBitis.Location = new System.Drawing.Point(148, 260);
            this.tabloVerileri8SatirBitis.Name = "tabloVerileri8SatirBitis";
            this.tabloVerileri8SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri8SatirBitis.TabIndex = 25;
            this.tabloVerileri8SatirBitis.Text = "00:00";
            // 
            // tabloVerileri7SatirBitis
            // 
            this.tabloVerileri7SatirBitis.Location = new System.Drawing.Point(148, 229);
            this.tabloVerileri7SatirBitis.Name = "tabloVerileri7SatirBitis";
            this.tabloVerileri7SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri7SatirBitis.TabIndex = 24;
            this.tabloVerileri7SatirBitis.Text = "00:00";
            // 
            // tabloVerileri6SatirBitis
            // 
            this.tabloVerileri6SatirBitis.Location = new System.Drawing.Point(148, 198);
            this.tabloVerileri6SatirBitis.Name = "tabloVerileri6SatirBitis";
            this.tabloVerileri6SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri6SatirBitis.TabIndex = 23;
            this.tabloVerileri6SatirBitis.Text = "00:00";
            // 
            // tabloVerileri5SatirBitis
            // 
            this.tabloVerileri5SatirBitis.Location = new System.Drawing.Point(148, 167);
            this.tabloVerileri5SatirBitis.Name = "tabloVerileri5SatirBitis";
            this.tabloVerileri5SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri5SatirBitis.TabIndex = 22;
            this.tabloVerileri5SatirBitis.Text = "00:00";
            // 
            // tabloVerileri4SatirBitis
            // 
            this.tabloVerileri4SatirBitis.Location = new System.Drawing.Point(148, 134);
            this.tabloVerileri4SatirBitis.Name = "tabloVerileri4SatirBitis";
            this.tabloVerileri4SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri4SatirBitis.TabIndex = 21;
            this.tabloVerileri4SatirBitis.Text = "00:00";
            // 
            // tabloVerileri3SatirBitis
            // 
            this.tabloVerileri3SatirBitis.Location = new System.Drawing.Point(148, 103);
            this.tabloVerileri3SatirBitis.Name = "tabloVerileri3SatirBitis";
            this.tabloVerileri3SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri3SatirBitis.TabIndex = 20;
            this.tabloVerileri3SatirBitis.Text = "00:00";
            // 
            // tabloVerileri2SatirBitis
            // 
            this.tabloVerileri2SatirBitis.Location = new System.Drawing.Point(148, 73);
            this.tabloVerileri2SatirBitis.Name = "tabloVerileri2SatirBitis";
            this.tabloVerileri2SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri2SatirBitis.TabIndex = 19;
            this.tabloVerileri2SatirBitis.Text = "00:00";
            // 
            // tabloVerileri1SatirBitis
            // 
            this.tabloVerileri1SatirBitis.Location = new System.Drawing.Point(148, 42);
            this.tabloVerileri1SatirBitis.Name = "tabloVerileri1SatirBitis";
            this.tabloVerileri1SatirBitis.Size = new System.Drawing.Size(71, 20);
            this.tabloVerileri1SatirBitis.TabIndex = 18;
            this.tabloVerileri1SatirBitis.Text = "00:00";
            // 
            // logText
            // 
            this.logText.Location = new System.Drawing.Point(259, 85);
            this.logText.Name = "logText";
            this.logText.Size = new System.Drawing.Size(218, 325);
            this.logText.TabIndex = 4;
            this.logText.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(376, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 70);
            this.button1.TabIndex = 5;
            this.button1.Text = "Gönder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(259, 9);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 70);
            this.button2.TabIndex = 6;
            this.button2.Text = "Getir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmZamanKisitTablosu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 424);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.logText);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.edtListeAdi);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.edtTabloAdi);
            this.Name = "frmZamanKisitTablosu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zaman Kısıt Tablosu";
            ((System.ComponentModel.ISupportInitialize)(this.edtTabloAdi)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown edtTabloAdi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edtListeAdi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tabloVerileri4SatirBaslangic;
        private System.Windows.Forms.TextBox tabloVerileri3SatirBaslangic;
        private System.Windows.Forms.TextBox tabloVerileri2SatirBaslangic;
        private System.Windows.Forms.TextBox tabloVerileri1SatirBaslangic;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tabloVerileri8SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri7SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri6SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri5SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri4SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri3SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri2SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri1SatirBitis;
        private System.Windows.Forms.TextBox tabloVerileri8SatirBaslangic;
        private System.Windows.Forms.TextBox tabloVerileri7SatirBaslangic;
        private System.Windows.Forms.TextBox tabloVerileri6SatirBaslangic;
        private System.Windows.Forms.TextBox tabloVerileri5SatirBaslangic;
        private System.Windows.Forms.RichTextBox logText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}